import React, {Component} from 'react';
import GoogleMapReact from "google-map-react";
import './mapContainer.scss';
//just for test ,remove 2 days later
const GOOGLE_KEY = 'AIzaSyCadK0RlGlRaiSej8RXOYgVW8DpTTvSPIM';
//the custom marker
const ShowMarkersComp = ({makerNum, currentMarkerNum}) =>
    <div id='mapContainer'>
        <div className='markerRadius' style={{backgroundColor: makerNum === currentMarkerNum ? 'red' : 'green'}}>
        </div>
        <span className="markerSize">
            Marker{makerNum}
        </span>
    </div>


export default class MapContainer extends Component {
    static defaultProps = {
        center: {lat: 30.67, lng: 104.07},
        zoom: 11,
    };
    state = {
        map: {},
        maps: {},
        datas: {},
    };

    componentDidUpdate() {
        this.renderPolygon();
    };

    //render the polygon
    renderPolygon() {
        const {datas, maps, map} = this.state;
        const {shpDataArr} = this.props;
        if (datas.length !== shpDataArr.length) {
            this.setState({
                datas: shpDataArr
            });
            const polygon = new maps.Polygon({
                paths: shpDataArr,
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0.35,
            });
            polygon.setMap(map);
        }
    }

    handleApiLoaded = (map, maps) => {
        this.setState({
            map, maps
        });
    };

    render() {
        const {markers, zoom, genMarkers, center, currentCenter, currentMarkerNum} = this.props;
        return (
            <GoogleMapReact
                bootstrapURLKeys={{key: GOOGLE_KEY}}
                defaultCenter={center}
                defaultZoom={zoom}
                onClick={genMarkers}
                center={currentCenter}
                yesIWantToUseGoogleMapApiInternals
                onGoogleApiLoaded={({map, maps}) => this.handleApiLoaded(map, maps)}
            >
                {/*re render when the map update*/}
                {
                    markers.map((item, index) => {
                        return <ShowMarkersComp
                            lat={item.lat}
                            lng={item.lng}
                            key={index}
                            makerNum={markers.length - index} //get marker number,since it's in reverse order
                            currentMarkerNum={currentMarkerNum}
                        />
                    })
                }
            </GoogleMapReact>
        )
    }
}
