import React, { Component } from 'react';
import { List, Button, Divider, Input } from 'antd';
import shp from "shpjs";
import './mapFunctional.scss';
import '../../utils/shpDatabase';
import { add2Db } from "../../utils/shpDatabase";

export default class MapFunctional extends Component {
    //convert the fileBuffer to shpJSON
    convert2Json = (arrayBuffer) => {
        let json = shp.parseShp(arrayBuffer);
        add2Db(json, this.props.changeLoading2False);
    };
    //handle the upload file
    handleFile = (e) => {
        let reader = new FileReader();
        let file = e.target.files[0];
        this.props.changeLoading2True();
        reader.onload = (upload) => {
            this.convert2Json(upload.target.result);
        };
        reader.readAsArrayBuffer(file);
    }

    render() {
        const { markers, clearBtnClick, listItemClick } = this.props;
        return (
            <div id="mapFunctional">
                <List
                    itemLayout="horizontal"
                    dataSource={markers}
                    renderItem={(item, index) => (
                        <List.Item onClick={() => {
                            listItemClick(item.lat, item.lng, markers.length - index)
                        }}>
                            <List.Item.Meta
                                title={<span className="markerTitle">Marker{markers.length - index}</span>}
                                description={<div
                                    className="markerTitle">lat:{item.lat.toFixed(2)},lng:{item.lng.toFixed(2)}</div>}
                            />
                        </List.Item>
                    )}
                    className="list"
                />
                <Divider />
                <div className="btnWrapper">
                    <Button type="primary" onClick={clearBtnClick} size="large" className="clearBtn">清除所有Markers</Button>
                    <Input type="file" onChange={this.handleFile} className="inputfile" />
                </div>
            </div>
        )
    }
}

