import Dexie from 'dexie';
import geohash from 'ngeohash';
const db = new Dexie("ShpDatabase");
//def the store data model
db.version(1).stores({ shpData: "++id,geoHash,bbox,coordinates,type" });
//add shp data to indexedDB
export const add2Db = (sourceData, isLoading) => {
    db.transaction('rw', db.shpData, () => {
        sourceData.forEach(shp => {
            //get the range's center pos
            let latCenter = (shp.bbox[1] + shp.bbox[3]) / 2
            let lngCenter = (shp.bbox[0] + shp.bbox[2]) / 2
            db.shpData.add({
                bbox: shp.bbox,
                coordinates: shp.coordinates,
                type: shp.type,
                geoHash: geohash.encode(latCenter, lngCenter, 9)
            })
        });
    }).catch(e => {
        alert(e.stack || e);
    }).then(e => {
        alert("data import to IndexedDB successful!");
        isLoading();
    });

}

