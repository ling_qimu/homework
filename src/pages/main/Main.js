import React, { Component } from 'react';
import MapContainer from "../../components/mapContainer/MapContainer.js";
import MapFunctional from "../../components/mapFunctional/MapFunctional.js";
import "./main.scss";
import { Spin } from 'antd';
import geohash from 'ngeohash';

let myWebWorker;

export default class Main extends Component {
    state = {
        markers: [],
        currentCenter: { lat: 30.67, lng: 104.07 },
        currentMarkerNum: 0,
        shpDataArr: [],
        isLoading: false
    };

    componentDidMount() {
        myWebWorker = new Worker("http://localhost:3000/worker.js")
        myWebWorker.onmessage = (res) => {
            //amazing!!!
            let posArr = [];
            if (res.data.length > 0) {
                for (const a of res.data) {
                    for (const arr1 of a.coordinates) {
                        for (const arr2 of arr1) {
                            for (const arr3 of arr2) {
                                if (arr3.length === 2) {
                                    posArr.push({
                                        lng: arr3[0],
                                        lat: arr3[1],
                                    })
                                }
                            }
                        }
                    }
                }
            }
            this.setState({
                shpDataArr: posArr
            });
        }

    }

    //generate the marker item
    genMarkers = async ({ lat, lng }) => {
        const { markers } = this.state;
        markers.unshift({
            lat: lat,
            lng: lng
        });
        this.setState({
            markers,
            currentCenter: { lat, lng },
            currentMarkerNum: markers.length,
        });
        //the length=4 the precision is about 39.1KM
        //todo graceful implement..
        let curGeoHash4 = geohash.encode(lat, lng, 4);
        let curGeoHash5 = geohash.encode(lat, lng, 5);
        let curGeoHash6 = geohash.encode(lat, lng, 6);
        let curGeoHash7 = geohash.encode(lat, lng, 7);
        myWebWorker.postMessage({ curGeoHash4, curGeoHash5, curGeoHash6, curGeoHash7 });
    };
    //click the clear button
    clearBtnClick = () => this.setState({ shpDataArr: [], markers: [] });
    //click the item
    listItemClick = (lat, lng, num) => this.setState({ currentCenter: { lat, lng }, currentMarkerNum: num });
    //change loading status
    changeLoading2True = () => this.setState({ isLoading: true });
    changeLoading2False = () => this.setState({ isLoading: false });

    render() {
        const { markers, currentCenter, currentMarkerNum, shpDataArr, isLoading } = this.state;
        return (
            <div id="main">
                <div className="mapContainer">
                    <MapContainer
                        markers={markers}
                        currentCenter={currentCenter}
                        genMarkers={this.genMarkers}
                        currentMarkerNum={currentMarkerNum}
                        shpDataArr={shpDataArr}
                    />
                </div>
                <div className="mapFunctional">
                    <MapFunctional
                        markers={markers}
                        clearBtnClick={this.clearBtnClick}
                        listItemClick={this.listItemClick}
                        changeLoading2True={this.changeLoading2True}
                        changeLoading2False={this.changeLoading2False}
                    />
                </div>
                <div className="loading">
                    <Spin size="large" spinning={isLoading} />
                </div>
            </div>
        )
    }
}
