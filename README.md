# 系统功能简述
## 使用的三方包
- antd 三方UI库
- dexie indexedDb工具包
- google-map-react googleMap React工具包
- ngeohash 计算geoHash的工具包
- shpjs 解析shapefile的工具包
## 基础功能
- 进入地图默认显示成都市中心的位置
- 使用者使用鼠标左键随意点击地图上任一位置后，地图会切换到以点击位置处为中心视角，同时在点击处生成一个自定义的Marker，并将此次记录记录到页面右侧的历史Marker栏中。
- 历史Marker的标记颜色为绿色，当前点击位置(中心点)标记颜色为红色，使用者可以通过点击历史Marker栏中的任意记录切换到不同的Marker点。
- 点击页面右下角的清除按钮，可以清除地图上所有Marker标记点并清除右侧历史Marker栏中数据
## shp渲染
- 点击页面右下角的选择文件按钮，选择对应的shpfile，使用shpjs解析为对应的JSON数据，然后存储到IndexedDb中
- 当用户切换当前center的时候，使用WebWorker查询当前中心点在IndexedDb存储数据中与之匹配的Bbox范围，并返回对应的区域数据,范围判断使用geoHash
- 查询到当前中心点附近的区域数据后，将数据转换成{lng:xxx,lat:yyy}对象类型的数组再调用Google原生的渲染多边形组件进行渲染
- 区域渲染存在部分问题，不能很好的通过中间点匹配渲染区域，同时使用geoHash设置的最低匹配精度是4位数字，存在地图缩放较大不能准确识别的问题，
应该可以使用zoom监听来匹配精度问题。