// eslint-disable-next-line no-restricted-globals,no-undef
importScripts('https://unpkg.com/dexie@latest/dist/dexie.js');
// eslint-disable-next-line no-restricted-globals
self.onmessage = async function (e) {
    const { curGeoHash4, curGeoHash5, curGeoHash6, curGeoHash7 } = e.data;
    // eslint-disable-next-line no-undef
    const db = new Dexie("ShpDatabase");
    db.version(1).stores({ shpData: "++id,geoHash,bbox,coordinates,type"});
    // Query
    let data = await db.shpData.where("id").aboveOrEqual(1).and((d) => {
        return d.geoHash.indexOf(curGeoHash4) >= 0;
    }).toArray()
    console.log(data)
    //Multi-layer filter
    if (data.length >= 1) {
        let temp = data;
        temp = temp.filter(d => d.geoHash.substring(0, 5) === curGeoHash5);
        if (temp.length >= 1) {
            data = temp;
            temp = temp.filter(d => d.geoHash.substring(0, 6) === curGeoHash6);
            if (temp.length >= 1) {
                data = temp;
                temp = temp.filter(d => d.geoHash.substring(0, 7) === curGeoHash7);
                if (temp.length >= 1) {
                    data = temp;
                }
            }
        }
    }
    postMessage(data);
}

